package main

import (
	"flag"
	"fmt"
	"path"
	"time"

	jwedownload "gitlab.com/B4dM4n/jwe-download"
)

func main() {
	fSecret := flag.String("secret", "", "file containing the shared key")
	fPath := flag.String("path", "", "the filesystem path to download")
	fFilename := flag.String("filename", "", "the download filename")
	fExpires := flag.Duration("expires", 48*time.Hour, "how long is the download link valid")
	fBase := flag.String("base", "https://avir.vps.fabianm.de/dl/", "download url base")

	flag.Parse()

	if *fFilename == "" {
		*fFilename = path.Base(*fPath)
	}

	params := jwedownload.NewParams(*fPath, *fFilename, *fExpires)

	secret, err := jwedownload.ReadSecretFile(*fSecret)
	if err != nil {
		fmt.Println("unable to read secret file:", err)
		return
	}

	serialized, err := params.Encrypt(secret)
	if err != nil {
		fmt.Println("unable to encrypt value:", err)
		return
	}
	fmt.Println(path.Join(*fBase, string(serialized)))
}

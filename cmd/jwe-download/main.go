package main

import (
	"archive/zip"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strconv"
	"syscall"

	jwedownload "gitlab.com/B4dM4n/jwe-download"
)

const (
	listenFdsStart = 3
)

type (
	handler struct {
		secret []byte
	}
)

func Listeners() ([]net.Listener, error) {
	files := ListenFds()
	listeners := make([]net.Listener, len(files))

	for i, f := range files {
		pc, err := net.FileListener(f)
		if err != nil {
			return nil, err
		}
		listeners[i] = pc
	}
	return listeners, nil
}

func ListenFds() []*os.File {
	pid, err := strconv.Atoi(os.Getenv("LISTEN_PID"))
	if err != nil || pid != os.Getpid() {
		return nil
	}
	nfds, err := strconv.Atoi(os.Getenv("LISTEN_FDS"))
	if err != nil || nfds == 0 {
		return nil
	}
	files := []*os.File(nil)
	for fd := listenFdsStart; fd < listenFdsStart+nfds; fd++ {
		syscall.CloseOnExec(fd)
		files = append(files, os.NewFile(uintptr(fd), ""))
	}
	return files
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.Body.Close()

	log.Printf("%s %s", r.RemoteAddr, r.URL.Path)
	params, err := jwedownload.Decrypt(h.secret, path.Base(r.URL.Path))
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintln(w, "invalid link:", err)
		log.Println("link decrypt:", err)
		return
	}
	err = params.Validate()
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintln(w, "invalid link:", err)
		log.Println("link validate:", err)
		return
	}
	stat, err := os.Stat(params.Path)
	if err != nil {
		w.WriteHeader(404)
		fmt.Fprintln(w, "Requested file not found")
		log.Println("filesystem path:", params.Path, err)
		return
	}
	if stat.IsDir() {
		filename := fmt.Sprintf("%s.zip", params.Filename)

		w.Header().Set("Content-Type", "application/zip")
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", filename))

		base := path.Dir(params.Path)
		zw := zip.NewWriter(w)

		err = filepath.Walk(params.Path, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.Mode().IsRegular() {
				header, _ := zip.FileInfoHeader(info)
				rel, err := filepath.Rel(base, path)
				if err != nil {
					return err
				}
				header.Name = rel
				header.Method = zip.Store
				fw, err := zw.CreateHeader(header)
				if err != nil {
					return err
				}
				f, err := os.Open(path)
				if err != nil {
					return err
				}
				defer f.Close()
				_, err = io.Copy(fw, f)
				if err != nil {
					return err
				}
			}
			return nil
		})
		if err != nil {
			log.Println("walk error:", params.Path, err)
		}
		if err := zw.Close(); err != nil {
			log.Println("close error:", params.Path, err)
		}
	} else {
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", params.Filename))
		http.ServeFile(w, r, params.Path)
	}
}

func main() {
	fSecret := flag.String("secret", "", "file copntaining the shared key")
	fListenNet := flag.String("listen-net", "tcp", "listener mode [unix,tcp]")
	fListen := flag.String("listen", "", "address socket to listen on")
	flag.Parse()

	secret, err := jwedownload.ReadSecretFile(*fSecret)
	if err != nil {
		log.Println("unable to read secret file:", err)
		return
	}

	listeners, err := Listeners()
	if err != nil {
		log.Println("unable to listen on passed fds:", err)
		return
	}
	if *fListen != "" {
		listener, err := net.Listen(*fListenNet, *fListen)
		if err != nil {
			log.Println("unable to listen:", err)
			return
		}
		listeners = append(listeners, listener)
	}
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)

	server := http.Server{
		Handler: &handler{secret},
	}
	for _, listener := range listeners {
		go func(listener net.Listener) {
			err := server.Serve(listener)
			if err != nil && err != http.ErrServerClosed {
				log.Println(err)
			}
		}(listener)
	}
	_ = <-ch
	server.Close()
}

#
# Builder
#
FROM golang:alpine as builder

COPY . /go/src/gitlab.com/B4dM4n/jwe-download
WORKDIR /go/src/gitlab.com/B4dM4n/jwe-download/cmd/jwe-download
RUN go build -i -v .

#
# Final stage
#
FROM alpine:latest
LABEL maintainer "FabianM <fabianm88@gmail.com>"

# install jwe-download
COPY --from=builder /go/src/gitlab.com/B4dM4n/jwe-download/cmd/jwe-download/jwe-download /usr/bin/jwe-download

ENTRYPOINT ["/usr/bin/jwe-download"]

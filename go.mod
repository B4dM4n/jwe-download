module gitlab.com/B4dM4n/jwe-download

require (
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf // indirect
	gopkg.in/square/go-jose.v2 v2.4.0
)

go 1.13

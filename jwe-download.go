package jwedownload

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"time"

	"github.com/pkg/errors"
	jose "gopkg.in/square/go-jose.v2"
)

type Params struct {
	Path     string
	Filename string
	Expires  time.Duration
	Created  time.Time
}

func NewParams(path string, filename string, expires time.Duration) *Params {
	return &Params{
		Path:     path,
		Filename: filename,
		Expires:  expires,
		Created:  time.Now(),
	}
}

func (p *Params) Encrypt(secret []byte) (string, error) {
	err := p.Validate()
	if err != nil {
		return "", errors.Wrap(err, "validate params")
	}
	encrypter, err := jose.NewEncrypter(jose.A128GCM, jose.Recipient{Algorithm: jose.DIRECT, Key: secret}, nil)
	if err != nil {
		return "", errors.Wrap(err, "create encrypter")
	}
	plaintext, err := json.Marshal(p)
	if err != nil {
		return "", errors.Wrap(err, "encode json")
	}
	object, err := encrypter.Encrypt(plaintext)
	if err != nil {
		return "", errors.Wrap(err, "encrypt content")
	}
	serialized, err := object.CompactSerialize()
	if err != nil {
		return "", errors.Wrap(err, "serialize content")
	}
	return serialized, nil
}

func Decrypt(secret []byte, text string) (*Params, error) {
	object, err := jose.ParseEncrypted(text)
	if err != nil {
		return nil, errors.Wrap(err, "parse content")
	}
	decrypted, err := object.Decrypt(secret)
	if err != nil {
		return nil, errors.Wrap(err, "decrypt content")
	}
	params := &Params{}
	err = json.Unmarshal(decrypted, params)
	if err != nil {
		return nil, errors.Wrap(err, "decode json")
	}
	return params, nil
}

func (p *Params) Validate() error {
	if !filepath.IsAbs(p.Path) {
		return errors.New("invalid path")
	}
	if p.Filename == "" {
		return errors.New("invalid filename")
	}
	now := time.Now()
	if p.Created.IsZero() || p.Created.After(now) {
		return errors.New("invalid create time")
	}
	if p.Expires <= 0 {
		return errors.New("invalid duration")
	}
	if p.Created.Add(p.Expires).Before(now) {
		return errors.New("expired")
	}
	return nil
}

func ReadSecretFile(filename string) ([]byte, error) {
	if filename == "" {
		return nil, errors.New("filename required")
	}
	secret, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, errors.Wrap(err, "read file")
	}
	if len(secret) < 16 {
		return nil, errors.New("secret to short")
	}
	return secret, nil
}
